package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.service.ICommandService;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.command.AbstractCommand;
import ru.t1.didyk.taskmanager.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService(){
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
