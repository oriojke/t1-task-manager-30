package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-id";
    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().removeById(userId, id);
    }

}
